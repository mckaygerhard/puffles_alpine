# Recommendations For Alpine Linux

These are my recommendations for the Alpine Linux distribution.

## Change the Init System to Runit

Runit is a suite of tools which include a PID 1 init as well as a daemontools-compatible process supervision framework, along with utilities which streamline creation and maintenance of services. Runit is an extremely fast, secure and small init system, I have used runit on several laptops (some as old as from 2005), on each laptop the average boot time was 1.5 to 2 seconds. It is extremely fast and very minimal, it is also very flexible. I strongly believe that Alpine should switch to runit, because it is fast and small (faster than OpenRC and much smaller than OpenRC). It also has full support for musl libc (a nice bonus).

## Reevaluate Base System

While I will agree that Alpine Linux is the smallest system out there (as far as I know), I also think it can be done a bit better. My view of a base system is that it should contain only things that are required for the system and you install everything else. For example, we have apropos (which searches man pages) but you don't really have any on the system by default and you need man to read them. Man gives you man -k which lets you search man pages, so why do we need apropos? I think we should reevaluate what all is in base and remove unneeded packages. My honest recommendation for a good base system would this:

	date
	du
	dd
	factor
	grep (with colors)
	ls (with colors)
	mk
	mkdir
	OpenBSD's PDKSH
	sha1sum
	sed (use sed instead of cat, maybe make a script in /bin called cat using sed -n p)
	touch (or vi)
	cp
	mv
	rm
	pwd
	echo
	cal


## Kernal Changes

This one is probably going to be the odd man out so to speak. I personally think we should take a serious look at the kernal, I think we should remove things like bluetooth (as in remove them from the kernal, delete the code for them from the kernal) as things like bluetooth are a serious security risk. However, some people may want these things thus we should write good code correct implementaions of these things and store them as patches that people can download and patch into the kernal when they install (more minimal and more secure!). I think we should do the same with hardware support etc.

For now this is all I have, please feel free to let me know what you think. Thank you for reading.
